<?php

namespace KDA\Laravel\Layouts\Aos;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Layouts\Facades\LayoutManager;
use Illuminate\Support\Facades\Blade;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    protected $packageName = 'laravel-layouts-aos';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
        LayoutManager::registerRenderHook(
            'head.styles',
            function () {
                return Blade::render('<link href="https://unpkg.com/aos@latest/dist/aos.css" rel="stylesheet">', []);
            }
        );
        LayoutManager::registerRenderHook(
            'head.scripts',
            function () {
                return Blade::render('<script src="https://unpkg.com/aos@latest/dist/aos.js"></script>', []);
            }
        );
        LayoutManager::registerRenderHook(
            'body.end',
            function () {
                return Blade::render('<script>AOS.init();</script>', []);
            }
        );
    }
}
